import 'package:flutter/material.dart';
import 'package:stacked_card_carousel/stacked_card_carousel.dart';

class RegHome extends StatelessWidget {
  RegHome({Key? key}) : super(key: key);

  final List<Widget> fancyCards = <Widget>[
    FancyCard(
      image: Image.asset("assets/images/1.png"),
      title: "1.การยื่นคำร้องขอสำเร็จการศึกษา ภาคปลาย ปีการศึกษา 2565(ด่วน)",
    ),
    FancyCard(
      image: Image.asset("assets/images/2.png"),
      title:
          "2.กำหนดการชำระค่าธรรมเนียมการลงทะเบียนเรียนภาคปลาย ปีการศึกษา 2565",
    ),
    FancyCard(
      image: Image.asset("assets/images/3.png"),
      title:
          "3.กำหนดยื่นคำร้องเกี่ยวกับงานพิธีพระราชทานปริญญาบัตรปีการศึกษา 2563 และปีการศึกษา 2564",
    ),
    FancyCard(
      image: Image.asset("assets/images/4.png"),
      title: "4.แบบประเมินความคิดเห็นต่อการให้บริการของสำนักงานอธิการบดี",
    ),
    FancyCard(
      image: Image.asset("assets/images/5.png"),
      title: "5.LINE Official",
    ),
    FancyCard(
      image: Image.asset("assets/images/6.png"),
      title: "6.Download ระเบียบสำหรับแนบเบิกค่าเล่าเรียนบุตร",
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: buildAppBar(),
        drawer: drawerReg(),
        body: StackedCardCarousel(items: fancyCards),
      ),
    );
  }
}

drawerReg() {
  return Drawer(
    child: Flexible(
      child: ListView(
        shrinkWrap: true,
        children: <Widget>[
          DrawerHeader(
            decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage('assets/images/buu_mobile.png'),
                  colorFilter: ColorFilter.mode(
                      Color.fromARGB(255, 124, 124, 124).withOpacity(0.4),
                      BlendMode.dstATop),
                  fit: BoxFit.cover),
            ),
            child: const Text(
              'เมนูหลัก',
              style: TextStyle(fontSize: 32),
            ),
          ),
          ListTile(
            hoverColor: Color.fromRGBO(253, 236, 150, 0.3),
            dense: true,
            trailing: Icon(
              Icons.show_chart,
              color: Colors.black,
            ),
            title: Text('ลงทะเบียน'),
            onTap: () {},
          ),
          Divider(
            color: Colors.grey,
          ),
          ListTile(
            hoverColor: Color.fromRGBO(253, 236, 150, 0.3),
            dense: true,
            trailing: Icon(
              Icons.bar_chart_rounded,
              color: Colors.black,
            ),
            title: Text('ผลการลงทะเบียน'),
            onTap: () {},
          ),
          Divider(
            color: Colors.grey,
          ),
          ListTile(
            hoverColor: Color.fromRGBO(253, 236, 150, 0.3),
            dense: true,
            trailing: Icon(
              Icons.description,
              color: Colors.black,
            ),
            title: Text('ผลอนุมัติเพิ่ม-ลด'),
            onTap: () {},
          ),
          Divider(
            color: Colors.grey,
          ),
          ListTile(
            hoverColor: Color.fromRGBO(253, 236, 150, 0.3),
            dense: true,
            trailing: Icon(
              Icons.event,
              color: Colors.black,
            ),
            title: Text('ตารางเรียน/สอบ'),
            onTap: () {},
          ),
          Divider(
            color: Colors.grey,
          ),
          ListTile(
            hoverColor: Color.fromRGBO(253, 236, 150, 0.3),
            dense: true,
            trailing: Icon(
              Icons.history_edu,
              color: Colors.black,
            ),
            title: Text('ประวัตินิสิต'),
            onTap: () {},
          ),
          Divider(
            color: Colors.grey,
          ),
          ListTile(
            hoverColor: Color.fromRGBO(253, 236, 150, 0.3),
            dense: true,
            trailing: Icon(
              Icons.book,
              color: Colors.black,
            ),
            title: Text('ผลการศึกษา'),
            onTap: () {},
          ),
          Divider(
            color: Colors.grey,
          ),
          ListTile(
            hoverColor: Color.fromRGBO(253, 236, 150, 0.3),
            dense: true,
            trailing: Icon(
              Icons.query_stats,
              color: Colors.black,
            ),
            title: Text('ตรวจสอบจบ'),
            onTap: () {},
          ),
          Divider(
            color: Colors.grey,
          ),
          Expanded(
            child: Align(
              alignment: FractionalOffset.bottomCenter,
              child: ListTile(
                hoverColor: Color.fromRGBO(253, 236, 150, 0.3),
                dense: true,
                trailing: Icon(
                  Icons.logout,
                  color: Colors.black,
                ),
                title: Text('Logout'),
                onTap: () {},
              ),
            ),
          ),
        ],
      ),
    ),
  );
}

AppBar buildAppBar() {
  return AppBar(
    backgroundColor: Color.fromRGBO(255, 215, 0, 1),
    title: Text(
      "RegBuu",
      style: TextStyle(fontWeight: FontWeight.bold),
    ),
    elevation: 10,
    toolbarHeight: 80.0,
    actions: <Widget>[
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Padding(
            padding: EdgeInsets.only(right: 20),
            child: ClipOval(
              child: Image.asset(
                'assets/images/arona.jpg',
                width: 55,
                height: 55,
                fit: BoxFit.cover,
              ),
            ),
          ),
        ],
      )
    ],
  );
}

class FancyCard extends StatelessWidget {
  const FancyCard({
    super.key,
    required this.image,
    required this.title,
  });

  final Image image;
  final String title;

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 4.0,
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          children: <Widget>[
            Container(
              width: 250,
              height: 250,
              child: image,
            ),
            Text(
              title,
              style: Theme.of(context).textTheme.headlineSmall,
            ),
            OutlinedButton(
              child: const Text("Learn more"),
              onPressed: () => print("Button was tapped"),
            ),
          ],
        ),
      ),
    );
  }
}
